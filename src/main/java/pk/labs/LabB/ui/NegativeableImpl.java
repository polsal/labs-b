package pk.labs.LabB.ui;

import javax.swing.JPanel;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.ControlPanel;

@Component("negative")
public class NegativeableImpl implements Negativeable{
    
        @Autowired
        private Utils util;
	private java.awt.Component component;
        
	@Override
	public void negative() {
            JPanel temp = ((ControlPanel) AopContext.currentProxy()).getPanel();
            util.negateComponent(temp);
	}	
}
