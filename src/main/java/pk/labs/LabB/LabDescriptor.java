package pk.labs.LabB;

    import pk.labs.LabB.Contracts.*;
    import pk.labs.LabB.controlpanel.ControlPanelBean;
    import pk.labs.LabB.display.DisplayBean;
    import pk.labs.LabB.main.EkspresBean;

public class LabDescriptor {

    
    // region P1
    public static String displayImplClassName = DisplayBean.class.getName();
    public static String controlPanelImplClassName = ControlPanelBean.class.getName();

    public static String mainComponentSpecClassName = Ekspres.class.getName();
    public static String mainComponentImplClassName = EkspresBean.class.getName();
    public static String mainComponentBeanName = "ekspres";
    // endregion

    // region P2
    public static String mainComponentMethodName = "ZrobKawe";
    public static Object[] mainComponentMethodExampleParams = new Object[] {"test"};
    // endregion

    // region P3
    public static String loggerAspectBeanName = "log";
    // endregion
}
